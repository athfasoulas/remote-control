import { Component, EventEmitter, Input, OnChanges, Output } from '@angular/core';

import {
  TableAction,
  TableActionOptions,
  TableCellFormatOptions,
  TableColumnSortOrderOptions,
  TableSetting,
} from './data-table.model';

@Component({
  selector: 'lib-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss'],
})
export class DataTableComponent implements OnChanges {
  @Input() public data: any[];
  @Input() public settings: TableSetting[];
  @Input() public loading: boolean;
  @Input() public sorting: string;
  @Input() public showPageSizeSelector: boolean;
  @Output() public actionClick: EventEmitter<TableAction> = new EventEmitter<TableAction>();
  public sortOrderOptions = TableColumnSortOrderOptions;
  public actionOptions = TableActionOptions;
  public formatOptions = TableCellFormatOptions;

  ngOnChanges(): void {
  }

  public showHead(): boolean {
    return !!this.settings?.find((x) => x.header !== undefined);
  }

  /**
   * Emit Table Action
   */
  public onAction(action: TableActionOptions, record: any, e?: MouseEvent): void {
    const tableAction = new TableAction();
    tableAction.action = action;
    tableAction.record = record;
    tableAction.e = e;
    this.actionClick.emit(tableAction);
  }


  /**
   * Update Sorting options
   */
  private changeSortOption(column: TableSetting): void {
    if (!column) {
      return;
    }

    this.settings.forEach((c) => {
      if (c.sorting) {
        c.sorting.selected = false;
      }
    });

    if (column?.sorting?.order === TableColumnSortOrderOptions.asc) {
      column.sorting.order = TableColumnSortOrderOptions.desc;
    } else {
      column.sorting.order = TableColumnSortOrderOptions.asc;
    }

    column.sorting.selected = true;
  }
}
