export class TableAction {
  public action: TableActionOptions;
  public record: any;
  public e: MouseEvent;
}

export enum TableActionOptions {
  edit,
  delete,
  details,
  check,
  execute,
  moreOptions,
  download,
}

export class TableSetting {
  public key: string;
  public header?: string;
  public visible?: boolean;
  public sorting?: TableColumnSortOptions;
  public actions?: TableActionOptions[];
  public format?: TableCellFormatOptions;
  public actionsVisibilityFunction?: (action: TableActionOptions, record: any) => boolean;
  public formatFunction?: (record: any) => void;
  public metricInline?: boolean;
  public styleFunction?: (record: any) => string;
  public moreOptions?: TableMoreOptions[];
  public hyperlinkFunction?: (record: any) => void;
}

export class TableMoreOptions {
  public id: number;
  public term: string;
  public enabled: boolean;
  public action: (record: any, selected: any) => void;
}
export class TableColumnSortOptions {
  public order: TableColumnSortOrderOptions;
  public selected: boolean;
  public name?: string;

  constructor(order: TableColumnSortOrderOptions, selected: boolean, name?: string) {
    this.order = order;
    this.selected = selected;
    this.name = name;
  }

  public stringFormat(columnPrimaryKey: string): string {
    const sortColumn = this.name ? this.name : columnPrimaryKey;
    return sortColumn + ' ' + TableColumnSortOrderOptions[this.order];
  }
}

export enum TableColumnSortOrderOptions {
  asc,
  desc,
}

export class TableActionItem {
  public action: TableActionOptions;
  public record: any;
  public e: MouseEvent;
}

export enum TableCellFormatOptions {
  default,
  string,
  checkIcon,
  dateTime,
  date,
  time,
  percentage,
  distance,
  speed,
  score,
  energyGJ,
  energyGJNormalized,
  energyMJ,
  energyMJNormalized,
  weight,
  custom,
  matIcon,
  hyperLink,
}
