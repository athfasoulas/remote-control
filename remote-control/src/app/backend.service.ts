import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';


export enum MovementType {
  Forward = 0,
  Backward = 1,
  Left = 2,
  Right = 3,
  Stop = 4,
  LightON = 5,
  LightOFF = 6,
  Honk = 7
}

@Injectable({
  providedIn: 'root'
})
export class BackendService {
  url = 'https://0b1gqpultb.execute-api.eu-central-1.amazonaws.com/Prod/Publisher/Publish';
  topic = 'hackathlon/kagkouras/guidance1';
  public commands : MovementType[] = [];

  constructor(private http: HttpClient) { }

  public sendCommand(type: MovementType) {

    this.commands.push( type);

    const payload = {
      'movementType': type,
      'topic': this.topic
    }

    return this.http.post(this.url, payload);
  }

}
