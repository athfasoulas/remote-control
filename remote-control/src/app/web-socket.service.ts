import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of, Subject, Subscription, throwError } from 'rxjs';
import { webSocket, WebSocketSubject } from 'rxjs/webSocket';

@Injectable({
  providedIn: 'root'
})
export class WebSocketService {
  public connected = new BehaviorSubject<null | boolean>(null);
  public messages = new Subject();
  
  private readonly RETRY_SECONDS = 5000;
  private wsSubject: WebSocketSubject<any>;
  private socketSubscription: Subscription;
  private socketStatusSubscription: Subscription;

  /**
   * Creates a new WebSocket subject, subscribes to it, inits reconnection process
   * and exposes messages to messages subject
   */
  public connect(url: string): void {
    if (!this.socketSubscription) {
      this.connected.next(null);
      this.reconnect(url);

      this.wsSubject = this.getWsSubject(url);
      this.socketSubscription = this.wsSubject.subscribe((data: any) => {
        this.messages.next(data);
      });
    }
  }

  /**
   * Send message
   */
  public sendMessage(data: any): Observable<boolean> {
    if (this.wsSubject) {
      this.wsSubject.next(data);
      return of(true);
    } else {
      return throwError('msg not sent, No connection found');
    }
  }

  /**
   * Close connection
   */
  public closeConnection(): void {
    this.clearSocketSubscription();
    this.clearSocketStatusSubscription();
  }

  /**
   * Clear subscription to socket
   */
  private clearSocketSubscription(): void {
    if (this.socketSubscription) {
      this.socketSubscription.unsubscribe();
      this.socketSubscription = null;
    }
  }

  /**
   * Clear subscription to socket status changes
   */
  private clearSocketStatusSubscription(): void {
    if (this.socketStatusSubscription) {
      this.socketStatusSubscription.unsubscribe();
      this.socketStatusSubscription = null;
    }
  }

  /**
   * Return a custom WebSocket subject
   */
  private getWsSubject(url: string): WebSocketSubject<any> {
    return webSocket({
      url,
      openObserver: {
        next: () => {
          this.connected.next(true);
          console.log('connection ok: ' + url);
        },
      },
      closeObserver: {
        next: () => {
          console.log('connection closed: ' + url);
          this.connected.next(false);
        },
      },
    });
  }

  /**
   * Subscribes to socket connection status and
   * reconnects if connection is failed or closed
   */
  private reconnect(url: string): void {
    this.socketStatusSubscription = this.connected.subscribe((status: boolean) => {
      if (status === false) {
        this.clearSocketSubscription();
        this.socketStatusSubscription.unsubscribe();
        console.log('reconnecting: ' + url);
        setTimeout(() => {
          this.connect(url);
        }, this.RETRY_SECONDS);
      }
    });
  }
}
