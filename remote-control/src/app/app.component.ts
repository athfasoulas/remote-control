import { Component, HostListener, OnInit } from '@angular/core';
import { timer } from 'rxjs';
import { BackendService, MovementType } from './backend.service';
import { TableSetting } from './data-table/data-table.model';
import { WebSocketService } from './web-socket.service';


export class Message{
   Aggregates : any[];
   Logs : Log[];
}

export class Log{
  Timestamp : string;
  Action : MovementType;
  DistanceFromObstacle : number;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  keydown = '';
  wssUrl = 'wss://l10f4jy4w5.execute-api.eu-central-1.amazonaws.com/development'
  logs : Log[] =[];
  blink = false;
  public commands : MovementType[] = [];

  public tableSettings: TableSetting[] =[
    {
      key: 'Timestamp',
      header: 'Timestamp',
    },
    {
      key: 'Action',
      header: 'Action',
    },
    {
      key: 'DistanceFromObstacle',
      header: 'DistanceFromObstacle',
    }
  ] ;

  constructor(private service: BackendService, private ws: WebSocketService) { }

  ngOnInit(): void {
    this.ws.connect(this.wssUrl);
    this.ws.messages.subscribe({
      next: (msg : any) => {
        this.blink = true;
        setTimeout(()=>{ this.blink = false;}, 3000)
        this.logs = msg.Logs;
      }
    })


    this.ws.sendMessage( {"action":"sendMessage", "DateTimeStart": "2021-12-10T10:00:00", "DateTimeEnd": "2022-12-12T10:30:00"} )
  }

  @HostListener('document:keyup', ['$event'])
  handleKeyupEvent(event: KeyboardEvent) {
    this.keydown = ''
    switch (event.code) {
      case 'KeyD':
        this.stop()
        break;
      case 'KeyW':
        this.stop()
        break;
      case 'KeyS':
        this.stop()
        break;
      case 'KeyA':
        this.stop()
        break;
      default:
        break;
    }
  }

  @HostListener('document:keypress', ['$event'])
  handleKeydownEvent(event: KeyboardEvent) {
    if (event.code === this.keydown) return;
    this.keydown = event.code
    switch (event.code) {
      case 'KeyD':
        this.right()
        break;
      case 'KeyW':
        this.forward()
        break;
      case 'KeyS':
        this.back()
        break;
      case 'KeyA':
        this.left()
        break;
      case 'Space':
        this.stop()
        break;
      default:
        break;
    }
  }

  public forward() {
    this.service.sendCommand(MovementType.Forward).subscribe()
  }

  public right() {
    this.service.sendCommand(MovementType.Right).subscribe()
  }

  public left() {
    this.service.sendCommand(MovementType.Left).subscribe()
  }

  public back() {
    this.service.sendCommand(MovementType.Backward).subscribe()
  }

  public stop() {
    this.service.sendCommand(MovementType.Stop).subscribe()
  }

  public lightsOn() {
    this.service.sendCommand(MovementType.LightON).subscribe()
  }

  public lightsOff() {
    this.service.sendCommand(MovementType.LightOFF).subscribe()
  }

  public doYourThing() {
    this.service.sendCommand(MovementType.LightOFF).subscribe()
  }

  public honk() {
    this.service.sendCommand(MovementType.Honk).subscribe()
  }


}
